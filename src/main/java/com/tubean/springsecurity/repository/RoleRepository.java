package com.tubean.springsecurity.repository;

import com.tubean.springsecurity.domain.Role;
import org.springframework.data.repository.CrudRepository;

public interface RoleRepository extends CrudRepository<Role, Integer> {
    Role findByName(String name);
}
