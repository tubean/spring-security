# spring-security

Tutorial about spring-security

- First, create new schema `demo_security`
- Then config username/password in order to access to database in `application.properties`
- Run script in `sql` folder
- Run application
- Using account `admin@gmail.com/123456` to access role admin
- Using account `member@gmail.com/123456` to access role member